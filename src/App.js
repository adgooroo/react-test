import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import UserManager from './components/UserManager';

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <h2>Welcome to React Test</h2>
        <p className="App-intro">
          To complete, the user must be able to view all the users from the users.json
          file and filter it by the username using the filter input.
        </p>
        <UserManager />
      </div>
    );
  }
}

export default App;
