import React, { Component } from 'react';
import UserList from './UserList';
import Filter from './Filter';
import users from '../users.json';

export default class UserManager extends Component {
	
	getUsers = () => users;	

	render() {
		return (
			<div>
				<Filter />
				<UserList users={this.getUsers()} />
			</div>
		);
	}
}