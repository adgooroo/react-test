import React from 'react';

export const Filter = () => {

	return (
		<div className="ui mini icon input">
			<input type="text" name="filter" placeholder="Filter..." />
			<i className="search icon"></i>
		</div>
	);
}

export default Filter;