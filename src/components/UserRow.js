import React from 'react';

export const UserRow = () => {
		return (
			<tr>
				<td className='firstName'>FirstName</td>
				<td className='lastName'>LastName</td>
				<td className='userName'>Username</td>
			</tr>
		)
}

export default UserRow;