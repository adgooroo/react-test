
import React from 'react';
import UserRow from './UserRow';

export const UserList = () => {

	const buildRows = () => {
		// do something
		return [];
	}

	return (
		<table className="ui celled table">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				{buildRows()}
			</tbody>
		</table>
	);
}

export default UserList;