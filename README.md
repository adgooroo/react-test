# React Test

## Description

To complete, the user must be able to view all the users from the users.json file and filter it by the username.

## Installation:

```base
$ npm install
$ npm start
```

## To Run Tests:
```base
$ npm test
```