import React from 'react';
import ReactDOM from 'react-dom';
import assert from 'assert';
import { mount } from 'enzyme';
import UserManager from '../src/components/UserManager';

describe('UserManager', () => {
	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<UserManager />, div);
	});
})