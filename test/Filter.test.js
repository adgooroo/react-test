import React from 'react';
import ReactDOM from 'react-dom';
import assert from 'assert';
import { shallow } from 'enzyme';
import Filter from '../src/components/Filter';

describe('Filter', () => {
    it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Filter />, div);
    });
    
    it('renders text', () => {
        const wrapper = shallow(<Filter text='abc' />);
        const elem = wrapper.find('input');
        assert.deepEqual(elem.props().value, 'abc');
    });
})
