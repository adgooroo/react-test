import React from 'react';
import ReactDOM from 'react-dom';
import assert from 'assert';
import { shallow } from 'enzyme';
import UserList from '../src/components/UserList';
import UserRow from '../src/components/UserRow';
describe('UserList', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<UserList />, div);
  });
  
  it('UserList default users prop is empty array', () => {
    const wrapper = shallow(<UserList />);
    assert.deepEqual(wrapper.instance().props.users, []);
  })
  
  it('UserList renders 5 rows', () => {
    const users = [
      {id:'1', firstName:'a', lastName:'b', username:'ab'},
      {id:'2', firstName:'c', lastName:'d', username:'cd'},
      {id:'3', firstName:'e', lastName:'f', username:'ef'},
      {id:'4', firstName:'g', lastName:'h', username:'gh'},
      {id:'5', firstName:'f', lastName:'g', username:'fg'},
    ];
    const wrapper = shallow(<UserList users={users} />);
    const rows = wrapper.find(UserRow);
    assert.deepEqual(rows.length, 5);
  });
  
  it('UserList renders 0 rows', () => {
    const users = [];
    const wrapper = shallow(<UserList users={users} />);
    const rows = wrapper.find(UserRow);
    assert.deepEqual(rows.length, 0);
  });
})